/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /*----init scrollReveal------------------*/
  $(document).ready(function () {
    var animate = {
      viewFactor: 0.2,
      useDelay: 'always',
      duration: 800,
      distance: "0px",
      reset: false,
      scale: 0.8,
    }
    window.sr = ScrollReveal(animate);
    sr.reveal('.animate');
  });

  /**
   * Initilize owl.carousel
   */
  Drupal.behaviors.sliders_on_homepage = {
    attach: function (context, settings) {
      $('.owl-one').owlCarousel({
        nav: true,
        items: 1,
        dots: false,
        loop: true,
      })
    }
  };

  Drupal.behaviors.partner_slider = {
    attach: function (context, settings) {

      $('.owl-two').owlCarousel({
        nav: false,
        items: 4,
        dots: false,
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        responsive: {
          0: {
            items: 2
          },
          640: {
            items: 3
          },
          768: {
            items: 4
          },
          1024: {
            items: 6
          }
        }
      })
    }
  };
  /**
   *  Scrolldown
   */
  Drupal.behaviors.scrollDown = {
    attach: function (context, settings) {
      $("#scroll-down").on('click', function () {
        $('html, body').animate({
          scrollTop: $("#main-wrapper").offset().top
        }, 1000);
      });

    }
  };


  /**
   * Menu toggle
   */
  Drupal.behaviors.menuToggle = {
    attach: function (context, settings) {

      $('#toggle_menu').unbind().on('click', function () {
        $(this).toggleClass('menu-close');
        $('#header').toggleClass('menu-is-open');

        if ($('#header').hasClass('menu-is-open')) {
          $("body").css("overflow", "hidden");
          $('html, body').css("height", "100%");
        } else {
          $("body").css("overflow", "initial");
          $('html, body').css("height", "initial");
        }
      });

      window.addEventListener('hashchange', offsetAnchor);
      window.setTimeout(offsetAnchor, 1);
      function offsetAnchor() {
        if (location.hash.length !== 0) {
          window.scrollTo(window.scrollX, window.scrollY - 400);
        }
      }

    }
  };


  /**
   * Map build with leaflet
   * https://github.com/Leaflet/Leaflet
   */
  Drupal.behaviors.map = {
    attach: function (context, settings) {
      $(context).find('#map').once('ifMap').each(function () {
        $.ajax({
          url: '/contact_data?_format=json',
          method: 'GET',
          success: function (data) {
            $.each(data, function (index, item) {
              var lat = item.latitude;
              var lng = item.longitude;
              var name = item.name;
              var street = item.street;
              var street_nr = item.house_nr;
              var postal = item.postal_code;
              var place = item.place;
              var company_nr = item.company_nr;

              var map = L.map('map',
                {
                  scrollWheelZoom: false
                }).setView([lat, lng], 15);

              L.tileLayer('https://api.mapbox.com/styles/v1/vincentmartensbe/cj9y6k4w37c4n2socq4c6tf9v/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidmluY2VudG1hcnRlbnNiZSIsImEiOiJjajl5Nmk0dWY2NTZtMndwaGt3eGFocmNrIn0.dnF52YcQejHsryyauRhIjw', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              }).addTo(map);

              // create custom icon
              var customIcon = L.icon({
                iconUrl: '/themes/base/images/icons/marker.min.svg',
                iconSize: [71, 100],
                iconAnchor: [35, 100],
                popupAnchor: [0, -80],
              });

              L.marker([lat, lng], {icon: customIcon}).addTo(map)
                .bindPopup('<span class="name">' + name + '</span>' + '</br>' +
                  '<span class="street">' + street + ' ' + street_nr + '</span>' + '</br>' +
                  '<span class="place">' + postal + ' ' + place + '</span>' + '</br>' +
                  '<span class="company">' + company_nr + '</span>');
            });
          }
        });
      });
    }
  };

  Drupal.behaviors.rental = {
    attach: function (context, settings) {
      $('#header-wrapper').clone().prependTo('#header-wrapper');
      var cart = $('#edit-totals');
      var cartPosition = cart.offset().top - 150;

      $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        var objectSelect = $(".view-mode-full");
        var objectPosition = objectSelect.offset().top - 250;
        if (scroll > objectPosition) {
          $('body').addClass('fixed-header-now');
        } else {
          $('body').removeClass('fixed-header-now');
        }

        if (scroll > cartPosition) {
          cart.addClass('fixed-now');
        } else {
          cart.removeClass('fixed-now');
        }

      });


      $("nav#block-verhuur-2 > ul.menu > li > a").each(function () {
        $(this).click(function () {
          //$('#block-navtoggle > button').trigger();
          $('#offCanvasRightOverlap').removeClass('is-open');
          $('#offCanvasRightOverlap').attr('aria-expanded', 'false');
          $('#block-navtoggle > button').attr('aria-expanded', 'false');
        });
      });

    }
  }

})(jQuery, Drupal);
