<?php

namespace Drupal\order_form\Plugin\views\style;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render an orderform with orderable items.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "order_item_form",
 *   title = @Translation("Orderitem form"),
 *   help = @Translation("Renders a from with nodes."),
 *   theme = "views_view_order_item_form",
 *   display_types = { "normal" }
 * )
 */
class OrderItemForm extends StylePluginBase
{

  protected $formBuilder;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }


  public function render()
  {
    $viewResult = $this->view->result;
    // Get the orderform.
    $form = $this->formBuilder->getForm('Drupal\order_form\Form\OrderItemForm', $viewResult);

    return $form;
  }
}
