<?php
/**
 * @file
 * Contains \Drupal\order_form\Form\OrderItemForm.
 */

namespace Drupal\order_form\Form;

use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\order_form\TaxonomyTermTree;
use Drupal\node\Entity\Node;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OrderItemForm extends FormBase
{

  protected $mailManager;
  protected $entityTypeManager;
  protected $taxonomyTermTree;

  public function __construct(MailManager $mailManager, EntityTypeManager $entityTypeManager, TaxonomyTermTree $taxonomyTermTree)
  {
    $this->mailManager = $mailManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->taxonomyTermTree = $taxonomyTermTree;
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('plugin.manager.mail'),
      $container->get('entity_type.manager'),
      $container->get('order_form.taxonomy_term_tree')
    );
  }

  public function getFormId()
  {
    return 'order_item_form';
  }

  /*
   * Build the form with the given data passed trough $viewResult.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $viewResult = null)
  {
    if ($viewResult) {

      $form['totals'] = [
        '#type' => 'fieldgroup',
        '#prefix' => '<a name="totals_overview" class="anchor_totals"></a>'
      ];
      $form['totals']['total'] = [
        '#type' => 'textfield',
        '#title' => t('Total'),
        '#disabled' => true,
      ];
      $form['totals']['days'] = [
        '#type' => 'number',
        '#default_value' => 1,
        '#min' => 1,
        '#step' => 1,
        '#title' => t('Huurperiode'),
      ];
      $form['totals']['request'] = [
        '#type' => 'button',
        '#value' => $this->t('Request price'),
      ];

      // Load hierarchical term tree.
      $tree = $this->taxonomyTermTree->load('categories');

      foreach ($tree as $key => $term) {
        // Do not include the first level of terms in the form.
        if ($term->parents[0] == "0") {
          $terms = $term->children;
          foreach ($terms as $tkey => $childTerm) {
            $name = $childTerm->name;
            $tid = $childTerm->tid;

            $form['term_' . $tid] = [
              '#type' => 'fieldgroup',
              '#title' => $name,
              '#attributes' => ['class' => 'parent-term-' . $term->tid],
              '#prefix' => '<a name="'.strtolower(str_replace(' ', '_', $name)).'"></a>'
            ];

            if ($childTerm->children) {
              $this->buildFieldGroups($form, $childTerm->children);
            }
          }
        }

      }

      /* @var ResultRow $result */
      foreach ($viewResult as $key => $result) {
        /* @var Entity $entity */
        $entity = $result->_entity;

        // Get the display render array of the entity.
        $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
        $build = $view_builder->view($entity, 'order_item');
        $output = render($build);

        // Current term of entity
        $entityTerm = $entity->field_category->entity;
        $entityTermId = $entityTerm->id();
        // Parent term of the term on the entity.
        $entityTermParent = $this->entityTypeManager->getStorage('taxonomy_term')->loadParents($entityTermId);
        $entityTermParent = reset($entityTermParent);
        $entityTermParentId = $entityTermParent->tid->value;
        $entityTermRootParents = $this->entityTypeManager->getStorage('taxonomy_term')->loadParents($entityTermParentId);
        if (empty($entityTermRootParents)) {
          $entityTermParent = null;
        }

        // Is there a parent on the term reference of the entity?
        if ($entityTermParent) {
          // Apply the correct nesting.
          $form['term_' . $entityTermParent->id()]['term_' . $entityTermId]['order_item_' . $entity->id()] = [
            '#type' => 'number',
            '#title' => $entity->label(),
            '#default_value' => 0,
            '#min' => 0,
            '#step' => 1,
            '#attributes' => ['class' => ['qty-input']],
            '#prefix' => '<div class="product-item">' . $output,
            '#suffix' => '</div>',
            '#required' => TRUE,
          ];

          if ($entity->hasField('field_price')) {
            $form['term_' . $entityTermParent->id()]['term_' . $entityTermId]['order_item_' . $entity->id()]['#attributes'] = ['class' => ['qty-input'], 'data-price' => $entity->get('field_price')->value];
          }

        } else {
          $form['term_' . $entityTermId]['order_item_' . $entity->id()] = [
            '#type' => 'number',
            '#title' => $entity->label(),
            '#default_value' => 0,
            '#min' => 0,
            '#step' => 1,
            '#attributes' => ['class' => ['qty-input']],
            '#prefix' => '<div class="product-item">' . $output,
            '#suffix' => '</div>',
            '#required' => TRUE,
          ];

          if ($entity->hasField('field_price')) {
            $form['term_' . $entityTermId]['order_item_' . $entity->id()]['#attributes'] = ['class' => ['qty-input'], 'data-price' => $entity->get('field_price')->value];
          }
        }
      }

      $form['end'] = [
        '#type' => 'fieldgroup'
      ];
      $form['end']['customer_information'] = [
        '#type' => 'fieldgroup'
      ];
      $form['end']['customer_information']['close'] = [
        '#type' => 'button',
        '#value' => $this->t('close'),
      ];
      $form['end']['customer_information']['first_name'] = [
        '#type' => 'textfield',
        '#title' => t('First name'),
        '#placeholder' => 'Voornaam *',
        '#required' => TRUE
      ];
      $form['end']['customer_information']['last_name'] = [
        '#type' => 'textfield',
        '#title' => t('Last name'),
        '#placeholder' => 'Naam *',
        '#required' => TRUE
      ];
      $form['end']['customer_information']['street'] = [
        '#type' => 'textfield',
        '#title' => t('Street'),
        '#placeholder' => 'Straat *',
        '#required' => TRUE
      ];
      $form['end']['customer_information']['nr'] = [
        '#type' => 'textfield',
        '#title' => t('Nr'),
        '#placeholder' => 'Nr. *',
        '#required' => TRUE
      ];
      $form['end']['customer_information']['postalcode'] = [
        '#type' => 'textfield',
        '#title' => t('Postal code'),
        '#placeholder' => 'Postcode *',
        '#required' => TRUE
      ];
      $form['end']['customer_information']['city'] = [
        '#type' => 'textfield',
        '#title' => t('City'),
        '#placeholder' => 'Stad *',
        '#required' => TRUE
      ];
      $form['end']['customer_information']['vat'] = [
        '#type' => 'textfield',
        '#title' => t('BTW Nummer'),
        '#placeholder' => 'BTW Nummer',
      ];
      $form['end']['customer_information']['company'] = [
        '#type' => 'textfield',
        '#title' => t('Bedrijfsnaam'),
        '#placeholder' => 'Bedrijfsnaam',
      ];
      $form['end']['customer_information']['email'] = [
        '#type' => 'email',
        '#title' => t('E-mail'),
        '#placeholder' => 'E-mail *',
        '#required' => TRUE
      ];
      $form['end']['customer_information']['date'] = [
        '#type' => 'textfield',
        '#title' => t('Datum event'),
        '#placeholder' => 'Datum event'
      ];
      $form['end']['customer_information']['delivery'] = [
        '#type' => 'radios',
        '#title' => t('Wenst u levering op locatie?'),
        '#options' => [
          0 => 'Ik haal het zelf op',
          1 => 'Laat het leveren'
        ],
        '#required' => TRUE
      ];
      $form['end']['customer_information']['message'] = [
        '#type' => 'textarea',
        '#title' => t('Extra opmerkingen'),
        '#placeholder' => 'Extra opmerkingen'
      ];
      $form['end']['customer_information']['actions'] = ['#type' => 'actions'];
      $form['end']['customer_information']['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Send'),
      ];

      $form['#attached']['library'][] = 'order_form/order_form';

      return $form;
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $orderItems = '<table><tr><th align="left">Item</th><th align="left">Aantal</th><th>Totaal</th></tr>';
    $formValues = $form_state->getValues();

    $orderTotal = 0;
    foreach ($formValues as $key => $value) {
      if ($value != '0') {
        if (strpos($key, 'order_item_') === 0) {
          $arr = explode('order_item_', $key);
          $entityId = $arr[1];

          $entity = Node::load($entityId);
          if ($entity->hasField('field_price')) {
            $unitPrice = $entity->get('field_price')->value;

            if ($unitPrice) {
              $price = $value * $unitPrice;
              $orderTotal += $price;

              $orderItems .= '<tr><td>' . $entity->getTitle() . '</td>' . '<td align="center">' . $value . '</td><td align="right">€ ' . $price . '</td></tr>';
            } else {
              $orderItems .= '<tr><td>' . $entity->getTitle() . '</td>' . '<td align="center">' . $value . '</td><td align="right"></td></tr>';
            }
          } else {
            $orderItems .= '<tr><td>' . $entity->getTitle() . '</td>' . '<td>' . $value . '</td></tr>';
          }
        }
      }
    }
    $orderItems .= '</table>';

    $days = $formValues['days'];

    if ($days > 1) {
      $orderItems .= '<p><strong>Huurperiode: </strong>' . $days . '</p>';
    }
    $orderItems .= '<p><strong>Totaalbedrag: </strong>€ ' . $orderTotal * $days . '</p>';

    $customerDetails = '<h3>Gegevens:</h3>'
      . $formValues['first_name'] . ' ' . $formValues['last_name'] . '<br>'
      . $formValues['street'] . ' ' . $formValues['nr'] . '<br>'
      . $formValues['postalcode'] . ' ' . $formValues['city'] . '<br>'
      . $formValues['email'] . '<br>';
    if ($formValues['vat']) {
      $customerDetails .= 'BTW: ' . $formValues['vat'] . '<br>';
    }
    if ($formValues['company']) {
      $customerDetails .= 'Bedrijf: ' . $formValues['company'] . '<br>';
    }
    if ($formValues['date']) {
      $customerDetails .= 'Datum event: ' . $formValues['date'] . '<br>';
    }
    if ($formValues['delivery'] == "1") {
      $customerDetails .= 'Levering: Laat het leveren<br>';
    } else if ($formValues['delivery'] == "0") {
      $customerDetails .= 'Levering: Ik kom het zelf ophalen<br>';
    }
    $customerDetails .= $formValues['message'];

    // Send mail to the admin.
    $key = 'order_notification';
    $to = \Drupal::config('system.site')->get('mail');
    $langcode = \Drupal::currentUser()->getPreferredAdminLangcode();
    $params['subject'] = t('An orderform has been submitted.');
    $params['message'] = '<h2>Volgende reservering werd geplaatst:</h2>' . html_entity_decode($orderItems) . html_entity_decode($customerDetails);

    $mail = $this->mailManager->mail('order_form', $key, $to, $langcode, $params, $form_state->getValue('email'), true);

    // Send mail to the visitor if necessary.
    $keyVisitor = 'order_confirmation';
    $toVisitor = $form_state->getValue('email');
    $paramsVisitor['subject'] = 'Uw aanvraag bij Feestplan';
    $paramsVisitor['message'] = '<p>Bedankt voor uw aanvraag op onze website van Feestplan. Wij contacteren u zo spoedig mogelijk terug ivm uw aanvraag. Dit was uw reservering:</p>' . html_entity_decode($orderItems) . '<p>Vriendelijke groeten <br> Feestplan</p>';
    $mail = $this->mailManager->mail('order_form', $keyVisitor, $toVisitor, $langcode, $paramsVisitor, $form_state->getValue('email'), true);

    //drupal_set_message(t('Thank you for your request. We will reply to you as soon as possible.'));
    $form_state->setRedirect('entity.node.canonical', array('node' => 411));
  }

  private function buildFieldGroups(&$form, $children)
  {
    foreach ($children as $key => $term) {

      $form['term_' . $term->parents[0]]['term_' . $term->tid] = [
        '#type' => 'fieldgroup',
        '#title' => $term->name
      ];

    }
  }

}
