##Display
The module creates a new display 'order_item'. You can template on that.

##Price handling
Module can be used with or without prices. If you wish to handle prices. Just add a field 'field_price' to the desired content type. If you wish not to use prices, remove the input fields in the form class and the behavior code in app.js.

##Categories
Add a field 'field_category' to the content type for category handling. Only 2 levels allowed!
If you want a custom handling of the categories, you can create your own hook_form_alter.