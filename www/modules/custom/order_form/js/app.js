(function ($, Drupal) {

    Drupal.behaviors.productQuantityIncrement = {
        attach: function (context, settings) {

            $("form .form-item-days.js-form-type-number").once('daysQuantityIncrement').append('<div class="inc qty-button">+</div><div class="dec qty-button">-</div>');
            $("form.order-item-form .product-item .js-form-type-number").once('productQuantityIncrement').append('<div class="inc qty-button">+</div><div class="dec qty-button">-</div>');

            $(".qty-button").once().click(function () {
                var $button = $(this);
                var oldValue = $button.parent().find("input").val();

                if ($button.text() == "+") {
                    var newVal = parseFloat(oldValue) + 1;
                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 0) {
                        var newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = 0;
                    }
                }
                $button.parent().find("input").val(newVal);
                update_amounts();

            });
        }
    };

    Drupal.behaviors.orderFormBehavior = {
        attach: function (context, settings) {

            $('fieldset[id^="edit-totals"]').once('messages-wrapper').append('<div class="order-form-alter"></div>');

            $('input[id^="edit-request"]').once('request-clicked').click(function(e){
                e.preventDefault();

                var amount = 0;
                $('input.qty-input').each(function(){
                    var qty = $(this).val();
                    amount += qty;
                });
                console.log(amount);
                if(amount > 0){
                    $('fieldset[id^="edit-customer-information"]').addClass('open');
                } else {
                    $('.order-form-alter').html('<p>Er werden geen artikelen geselecteerd.</p>');
                    $('.order-form-alter').fadeIn();
                    setTimeout(function(){
                        $('.order-form-alter').fadeOut();
                    }, 2000);
                }

            });

            $('input[id^="edit-close"]').once('close-clicked').click(function(e){
                e.preventDefault();

                $('fieldset[id^="edit-customer-information"]').removeClass('open');
            });

            update_amounts();
            $('input[type="number"]').change(function() {
                update_amounts();
            });
            $('input[type="number"]').keyup(function() {
                update_amounts();
            });
        }
    };

    function update_amounts()
    {
        var sum = 0.00;
        var days = $('.form-item-days input').val();
        $('input.qty-input').each(function(){
            var price = $(this).data('price');

            if(typeof price != 'undefined'){
                var qty = $(this).val();
                var amount = (qty*price);
                sum += amount;
            }
        });
        sum = sum * days;
        $('input[id^="edit-total"]').val('€' + sum.toFixed(2));
    }

})(jQuery, Drupal);
